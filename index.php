<?php
require 'lib/http_response_code.inc.php';

$allowed_pages = array('home','about','products','login','contact');

if(isset($_GET['page'])){
    $page  = $_GET['page'];
    } else {
    $page = 'home';
    }

    $tmp_templates = "templates/";


if(!in_array($page, $allowed_pages)){
   http_response_code(404);
   $page = 'error404';
   include $tmp_templates.$page.".php";
   //include "templates/error404.php";
}

  $view_path     = $tmp_templates.$page.".php";

if(file_exists($view_path)){
    //include $tmp_templates.$page.".php";
    include $view_path;
} else{
    http_response_code(500);
    $page = 'error500';
    include $tmp_templates.$page.".php";
}



?>
